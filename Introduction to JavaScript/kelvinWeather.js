// temperrature in kelvin
const kelvin = 0;
//converting from kelvin to degree
const celsius = kelvin-273;
// conversion from celsius to fahrenheit
let fahrenheit = celsius*(9/5) + 32;
// rounding off
fahrenheit = Math.floor(fahrenheit);
console.log(`The temperature is ${fahrenheit} degrees Fahrenheit`);