// Assigning age
const myAge = 21;
// Assigning early years
let earlyYears = 2;
earlyYears = earlyYears * 10.5;
// substracting 2 from myAge
let laterYears = myAge - 2;
laterYears = laterYears * 4;
const myAgeInDogYears = earlyYears + laterYears;
const myName = 'Abhishek'.toLowerCase();
console.log(`My name is ${myName}. I am ${myAge} years old in human years which is ${laterYears} years old in dog years`);