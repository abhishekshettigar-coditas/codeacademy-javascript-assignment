//length
const objectives = ['Learn a new language', 'Read 52 books', 'Run a marathon'];
console.log(objectives.length);

// push

const chores = ['wash dishes', 'do laundry', 'take out trash'];
chores.push('cook food', 'netflix and chill');
console.log(chores);

//pop

const chores1 = ['wash dishes', 'do laundry', 'take out trash', 'cook dinner', 'mop floor'];
chores.pop();
console.log(chores);

//slice

const groceryList = ['orange juice', 'bananas', 'coffee beans', 'brown rice', 'pasta', 'coconut oil', 'plantains'];

groceryList.shift()
console.log(groceryList);
groceryList.unshift('popcorn')
console.log(groceryList);

console.log(groceryList.slice(1, 4))
console.log(groceryList);

const pastaIndex = groceryList.indexOf('pasta');
console.log(pastaIndex);

